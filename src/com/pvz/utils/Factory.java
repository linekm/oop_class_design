package com.pvz.utils;

import java.awt.Rectangle;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;

import com.pvz.gamemodel.bullet.Bullet;
import com.pvz.gamemodel.bullet.BulletType;
import com.pvz.gamemodel.plants.*;
import com.pvz.gamemodel.zombies.*;
import com.pvz.config.*;
import com.pvz.props.*;
import com.pvz.mainframe.*;


public class Factory {
    public static int cherryboom = 0;

    public static void createPlantsProp(Plants p, MyPoint gpt) {
        Sun s = new Sun();
        p.Count();
        //System.out.println(isZombieAppear(p));
        if ((p.getPlantstype() == PlantsType.PEASHOOTER || p.getPlantstype() == PlantsType.REPEATER) && (p.getT() >= 200) && (isZombieAppear(p) == true)) {
            Bullet b = new Bullet();
            if (p.getAdatk() == 1) {
                b.setAtk(15);
                b.setGifImage(ResourceUtils.GIFIMAGES.get("PB10.gif").getImage());
            } else {
                b.setGifImage(ResourceUtils.GIFIMAGES.get("PB01.gif").getImage());
                b.setAtk(10);
            }
            b.setHeight(b.getGifImage().getHeight(null));
            b.setWidth(b.getGifImage().getWidth(null));
            b.setSpeed(5);
            b.setX(gpt.getX() + 65);
            b.setY(gpt.getY() + 3);
            b.setBulletType(BulletType.PEA);

            p.getBulletsList().put(b, gpt);
            if (p.getPlantstype() == PlantsType.REPEATER && p.getAdspeed() == 1) {
                p.setT(150);
            } else if (p.getPlantstype() == PlantsType.PEASHOOTER && p.getAdspeed() == 1) {
                p.setT(100);
            } else if (p.getPlantstype() == PlantsType.REPEATER && p.getAdspeed() != 1) {
                p.setT(100);
            } else if (p.getPlantstype() == PlantsType.PEASHOOTER && p.getAdspeed() != 1) {
                p.setT(0);
            }

        } else if (p.getPlantstype() == PlantsType.ICEPEA && (p.getT() >= 200) && (isZombieAppear(p) == true)) {
            Bullet b = new Bullet();
            b.setGifImage(ResourceUtils.GIFIMAGES.get("SnowPeashooterBullet.gif").getImage());
            b.setHeight(b.getGifImage().getHeight(null));
            b.setWidth(b.getGifImage().getWidth(null));
            b.setSpeed(5);
            b.setX(gpt.getX() + 65);
            b.setY(gpt.getY() + 3);
            b.setBulletType(BulletType.ICE_PEA);
            b.setAtk(10);
            p.getBulletsList().put(b, gpt);
            if (p.getAdspeed() == 1) {
                p.setT(100);
            } else {
                p.setT(0);
            }

        } else if (p.getPlantstype() == PlantsType.SUNFLOWER && p.getT() >= 1000) {
            s.setGifImage(ResourceUtils.GIFIMAGES.get("Sun.gif").getImage());
            s.setHeight(s.getGifImage().getHeight(null));
            s.setWidth(s.getGifImage().getWidth(null));
            s.setX(gpt.getX());
            s.setY(gpt.getY());
            p.setT(0);
            SunFlower.FlowerSunList.put(s, gpt);
        }

    }

    public static void ZbMove() {
        for (int i = 0; i < LoadingPlants.LoadingZombieLists.size(); i++) {
            Zombie zb = LoadingPlants.LoadingZombieLists.get(i);
            if (!zb.isDeathing() && zb.getDie() != 1) {
                zb.moveZombie();
            }
            Set<Entry<Plants, MyPoint>> entrySet = DrawPlantsInMap.PlantsMap.entrySet();
            for (Entry<Plants, MyPoint> entry : entrySet) {
                // System.out.println(entry.getKey().GetRectangle());
                if (zb.GetRectangle().intersects(entry.getKey().GetRectangle()) && entry.getKey().getPlantstype() != PlantsType.CHERRYBOOM) {
                    zb.setX(entry.getKey().getX() + 35);
                    if (zb.getZbType() == ZombieType.Conehead) {
                        zb.setGifImage(ResourceUtils.GIFIMAGES.get("ConeheadZombieAttack.gif").getImage());
                    } else if (zb.getZbType() == ZombieType.NORMAL) {
                        zb.setGifImage(ResourceUtils.GIFIMAGES.get("ZombieAttack.gif").getImage());
                    }
                    int hp = entry.getKey().getHP();
                    hp -= zb.getZombieAtk();
                    Zombie z = new Zombie();
                    z.getEatzombie().put(entry.getKey(), zb);
                    Set<Entry<Plants, Zombie>> entrySet2 = z.getEatzombie().entrySet();
                    for (Entry<Plants, Zombie> entry2 : entrySet2) {

                        if (entry2.getKey().getHP() <= 0) {
                            DrawPlantsInMap.PlantsMap.remove(entry.getKey());
                            System.out.println(entry2.getValue());
                            if (entry2.getValue().getZbType() == ZombieType.Conehead) {
                                //System.out.println("CATCATCATCATCATCAT");
                                zb.setGifImage(ResourceUtils.GIFIMAGES.get("ConeheadZombie.gif").getImage());
                                zb.setWidth(ResourceUtils.GIFIMAGES.get("ConeheadZombie.gif").getIconWidth());
                                zb.setHeight(ResourceUtils.GIFIMAGES.get("ConeheadZombie.gif").getIconHeight());
                            } else if (entry2.getValue().getZbType() == ZombieType.NORMAL) {
                                //System.out.println("NORMALNORMALNORMALNORMALNORMALNORMAL");
                                zb.setGifImage(ResourceUtils.GIFIMAGES.get("Zombie.gif").getImage());
                                zb.setWidth(ResourceUtils.GIFIMAGES.get("Zombie.gif").getIconWidth());
                                zb.setHeight(ResourceUtils.GIFIMAGES.get("Zombie.gif").getIconHeight());
                            }
                        } else {
                            entry.getKey().setHP(hp);
                        }
                    }

                }

                Rectangle r = new Rectangle(PlantsVSZombies.boomX - 100, PlantsVSZombies.boomY - 100, 200, 200);
                if (zb.GetRectangle().intersects(r) && entry.getKey().getPlantstype() == PlantsType.CHERRYBOOM) {
//						zb.setGifImage(ResourceUtils.GIFIMAGES.get("BoomDie2.gif").getImage());
//						zb.setWidth(ResourceUtils.GIFIMAGES.get("BoomDie2.gif").getIconWidth());
//						zb.setHeight(ResourceUtils.GIFIMAGES.get("BoomDie2.gif").getIconHeight());
                    cherryboom = 1;
                    zb.setDie(1);
                    zb.setDeathing(true);
                    Timer t = new Timer();
                    t.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            LoadingPlants.LoadingZombieLists.remove(zb);

                        }
                    }, 1000);
                }

            }
        }
    }

    public static boolean isZombieAppear(Plants p) {
        boolean f = false;
        for (int i = 0; i < LoadingPlants.LoadingZombieLists.size(); i++) {
            Zombie zb = LoadingPlants.LoadingZombieLists.get(i);
            if ((zb.getX() <= Config.WINDOWS_WIDTH - 20) && p.getX() <= zb.getX() && p.getY() == (zb.getY() + zb.getHeight() / 2)) {
                f = true;
            }
        }
        return f;
    }

    public int ColdTime(Plants p, GameState gameState) {
        int coldt = p.getColdSpeed();
        if (gameState == GameState.GAMING || gameState == GameState.LEVEL1) {
            coldt++;
            if (coldt >= Plants.GetColdTime(p) && coldt <= Plants.GetColdTime(p) + Plants.GetColdSpeed(p)) {
                p.setColdSpeed(coldt);
                return 6;
            } else if (coldt > Plants.GetColdTime(p) + Plants.GetColdSpeed(p) && coldt <= Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 2) {
                p.setColdSpeed(coldt);
                return 5;
            } else if (coldt > Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 2 && coldt <= Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 3) {
                p.setColdSpeed(coldt);
                return 4;
            } else if (coldt > Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 3 && coldt <= Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 4) {
                p.setColdSpeed(coldt);
                return 3;
            } else if (coldt > Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 4 && coldt <= Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 5) {
                p.setColdSpeed(coldt);
                return 2;
            } else if (coldt > Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 5 && coldt <= Plants.GetColdTime(p) + Plants.GetColdSpeed(p) * 6) {
                p.setColdSpeed(coldt);
                return 1;
            } else {
                p.setColdSpeed(coldt);
                return 0;
            }
        } else {
            return -1;
        }
    }


    public static void bulletAtkZombie(Zombie zb, Bullet b) {
        int hp = zb.getHP();
        //System.out.println(hp+"------"+b.getKey().getAtk());
        Random r = new Random();
        hp -= b.getAtk();
        if (b.getBulletType() == BulletType.ICE_PEA) {
            zb.setSpeed(50);
        }
        int rr = r.nextInt(100);
        if (hp <= 0) {
            if (rr >= 0 && rr < 30) {
                Prop prop = new Prop();
                prop.setX(zb.getX() + 60);
                prop.setY(zb.getY() + 60);
                prop.setImage(ResourceUtils.IMAGES.get("Heal.png"));
                prop.setWidth(ResourceUtils.IMAGES.get("Heal.png").getWidth());
                prop.setHeight(ResourceUtils.IMAGES.get("Heal.png").getHeight());
                prop.setPropType(PropType.HEAL);
                Prop.propList.add(prop);
            } else if (rr >= 30 && rr <= 60) {
                Prop prop = new Prop();
                prop.setX(zb.getX() + 60);
                prop.setY(zb.getY() + 60);
                prop.setImage(ResourceUtils.IMAGES.get("addatk.png"));
                prop.setWidth(ResourceUtils.IMAGES.get("addatk.png").getWidth());
                prop.setHeight(ResourceUtils.IMAGES.get("addatk.png").getHeight());
                prop.setPropType(PropType.ADDATK);
                Prop.addAtkPropList.add(prop);
            } else {
                Prop prop = new Prop();
                prop.setX(zb.getX() + 60);
                prop.setY(zb.getY() + 60);
                prop.setImage(ResourceUtils.IMAGES.get("addspeed.png"));
                prop.setWidth(ResourceUtils.IMAGES.get("addspeed.png").getWidth());
                prop.setHeight(ResourceUtils.IMAGES.get("addspeed.png").getHeight());
                prop.setPropType(PropType.ADDSPEED);
                Prop.addSpeedPropList.add(prop);
            }

            zb.setDeathing(true);
            zb.setZombieDie(ResourceUtils.GIFIMAGES.get("ZombieDie.gif").getImage());
            zb.setWidth(ResourceUtils.GIFIMAGES.get("ZombieDie.gif").getIconWidth());
            zb.setHeight(ResourceUtils.GIFIMAGES.get("ZombieDie.gif").getIconHeight());
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        LoadingPlants.LoadingZombieLists.remove(zb);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        } else {
            zb.setHP(hp);
        }
    }

}