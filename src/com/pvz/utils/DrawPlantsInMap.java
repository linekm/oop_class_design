package com.pvz.utils;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map.Entry;

import com.pvz.gamemodel.plants.Plants;


public class DrawPlantsInMap {
    public static ArrayList<Plants> PlantsLists = new ArrayList<>();

    //IdentityHashMap  Key值可以重复的map集合
    //public static  IdentityHashMap<Plants, MyPoint> PlantsMap = new IdentityHashMap<Plants, MyPoint>();
    public static ConcurrentHashMap<Plants, MyPoint> PlantsMap = new ConcurrentHashMap<Plants, MyPoint>();

    public static ArrayList<Plants> getPlantsLists() {
        return PlantsLists;
    }

    public static void setPlantsLists(ArrayList<Plants> plantsLists) {
        PlantsLists = plantsLists;
    }

    public static void AcdMapCreatBullet() {
        Set<Entry<Plants, MyPoint>> entrySet = DrawPlantsInMap.PlantsMap.entrySet();
        for (Entry<Plants, MyPoint> entry : entrySet) {
            Factory.createPlantsProp(entry.getKey(), entry.getValue());
        }
    }


    /**
     * 判断区域是否种过植物
     *
     * @param getp
     * @return
     */
    public static boolean IsExitPlant(MyPoint getp) {
        Set<Entry<Plants, MyPoint>> entrySet = DrawPlantsInMap.PlantsMap.entrySet();
        boolean f = true;
        for (Entry<Plants, MyPoint> entry : entrySet) {
            //System.out.println(entry.getValue().getX()+"--------------"+entry.getValue().getY());
            if ((entry.getValue().getX() == getp.getX() && entry.getValue().getY() == getp.getY())) {
                f = false;
            }
        }
        return f;
    }
}