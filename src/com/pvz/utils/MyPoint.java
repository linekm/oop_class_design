package com.pvz.utils;

import com.pvz.config.*;

import java.awt.*;

public class MyPoint {
    private int x;
    private int y;

    public MyPoint(int x, int y) {
        setX(x);
        setY(y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}