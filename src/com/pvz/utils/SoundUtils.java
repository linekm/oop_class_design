package com.pvz.utils;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.swing.SwingWorker;

import com.pvz.config.*;
import com.pvz.mainframe.*;


public class SoundUtils {


    public static void Play(final File f, final boolean loop, GameState gameState) {
        new Thread() {
            @Override
            public void run() {
                SoundUtils.playMusic(loop, f, gameState);
            }
        }.start();

    }

    public static void playMusic(boolean loop, File file, GameState gameState) {
        byte[] audioData = new byte[1024];

        AudioInputStream ais = null;
        SourceDataLine line = null;
        try {//获取音频输入流
            ais = AudioSystem.getAudioInputStream(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ais != null) {
            //获取音频的编码对象
            AudioFormat baseFormat = ais.getFormat();
            //包装音频信息
            DataLine.Info info = new DataLine.Info(SourceDataLine.class,
                    baseFormat);
            try {
                //使用包装音频信息后的Info类创建源数据行，充当混频器的源
                line = (SourceDataLine) AudioSystem.getLine(info);
                line.open(baseFormat);  // open input
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            }
        }
        if (line == null) {
            return;
        }

        line.start();  // start output
        int inByte = 0;
        while (inByte != -1) {

            try {
                //通过数据行读取音频数据流，发送到混音器;
                //数据流传输过程：AudioInputStream -> SourceDataLine;
                inByte = ais.read(audioData, 0, 1024);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            try {
                if (inByte > 0) {
                    line.write(audioData, 0, inByte);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (gameState != PlantsVSZombies.gameState) {
                break;
            }

        }
        line.drain();  // drop remain

        line.stop();  // stop input

        line.close();  // close output
        if (loop) {
            playMusic(loop, file, gameState);
        }
    }


}