package com.pvz.utils;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import com.pvz.gamemodel.plants.*;
import com.pvz.gamemodel.zombies.*;
import com.pvz.mainframe.*;
import com.pvz.config.*;

public class LoadingPlants {

    public static ArrayList<Plants> PlantsList = new ArrayList<Plants>();
    public static ArrayList<Zombie> LoadingZombieLists = new ArrayList<Zombie>();
    public static ArrayList<Zombie> ZombieLists = new ArrayList<Zombie>();
    public static ArrayList<Plants> PlayerGetPlantsLists = new ArrayList<Plants>();
    public static int zombiesize;

    static {

        Peashooter peashooter = new Peashooter();
        //	PlantsList.add(new Repeater());
        PlantsList.add(new SunFlower());
        PlantsList.add(peashooter);
        PlantsList.add(new Repeater());
        PlantsList.add(new SnowPea());
        PlantsList.add(new CherryBoom());
        PlantsList.add(new Walnut());
    }

    public static void CreatZombie() {

        if (PlantsVSZombies.gameState == GameState.LEVEL1) {
            LoadingZombieLists.add(new ConeheadZombie(Config.WINDOWS_WIDTH + 10));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 0));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 220));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 260));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 280));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 360));
            LoadingZombieLists.add(new ConeheadZombie(Config.WINDOWS_WIDTH + 400));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 450));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 520));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 530));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 600));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 670));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 700));
            zombiesize = 13;
        } else if (PlantsVSZombies.gameState == GameState.LEVEL2) {
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 200));
            LoadingZombieLists.add(new NormalZombie(Config.WINDOWS_WIDTH + 200));
        }

    }

    public static void PlayerGetPlants() {
        //PlayerGetPlantsLists.add();
    }

    int n = 0;

    public static Rectangle sunflowerRect = new Rectangle(310, 163, 55, 80);
    public static Rectangle wandouRect = new Rectangle(375, 163, 55, 80);
    public static Rectangle DoubleWaRect = new Rectangle(440, 163, 55, 80);
    public static Rectangle snowPea = new Rectangle(505, 163, 55, 80);
    public static Rectangle cherryboom = new Rectangle(570, 163, 55, 80);
    public static Rectangle walnut = new Rectangle(700, 163, 55, 80);

    /**
     * 当玩家 点击某植物时  切换成灰色图片
     *
     * @param r
     * @param e
     * @return
     */
    public int GetPlantsInt(Rectangle r, MouseEvent e) {

        if (sunflowerRect.contains(e.getPoint())) {
            n = 0;
        } else if (wandouRect.contains(e.getPoint())) {
            n = 1;
        } else if (DoubleWaRect.contains(e.getPoint())) {
            n = 2;
        } else if (snowPea.contains(e.getPoint())) {
            n = 3;
        } else if (cherryboom.contains(e.getPoint())) {
            n = 4;
        } else if (walnut.contains(e.getPoint())) {
            n = 6;
        }
        return n;
    }

    static LoadingPlants l = new LoadingPlants();

    public static void ChoicedPlants(int i, Rectangle r, Plants p, MouseEvent e) {
        if (r.contains(e.getPoint())) {
            SoundUtils.Play(ResourceUtils.AUDIO.get("card.wav"), false, GameState.CHOOSE_PLANTS);
            if (!PlantsBar.PlantsBarList.contains(p)) {
                PlantsBar.PlantsBarList.add(p);
                //System.out.println(p);
                LoadingPlants.PlantsList.get(l.GetPlantsInt(r, e)).setChoiced(true);
                i = 1;
            }
            if (i == 0) {
                PlantsBar.PlantsBarList.remove(p);
                //System.out.println(p);
                LoadingPlants.PlantsList.get(l.GetPlantsInt(r, e)).setChoiced(false);
            }
            //System.out.println(i);
        }
    }


    int m = 0;

    public int ChocedGuanQia(MouseEvent e) {
        Rectangle Level1 = new Rectangle(258, 141, 77, 30);
        Rectangle Level2 = new Rectangle(357, 141, 77, 30);
        Rectangle Level3 = new Rectangle(456, 141, 77, 30);
        Rectangle Level4 = new Rectangle(555, 141, 77, 30);
        Rectangle Level5 = new Rectangle(654, 141, 77, 30);
        if (Level1.contains(e.getPoint())) {
            m = 1;
        } else if (Level2.contains(e.getPoint())) {
            m = 2;
        } else if (Level3.contains(e.getPoint())) {
            m = 3;
        } else if (Level4.contains(e.getPoint())) {
            m = 4;
        } else if (Level5.contains(e.getPoint())) {
            m = 5;
        }
        return m;
    }
}