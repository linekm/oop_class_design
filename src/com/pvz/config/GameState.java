/*
 *游戏场景标识枚举
 */
package com.pvz.config;

public enum GameState {
    WELCOME,
    MAIN_SCREEN,
    SELECT_LEVEL,
    GAMING,
    WIN,
    GAMEOVER,
    ILLUSTRATION,
    CHOOSE_PLANTS,
    MAP_MOVE_RIGHT,
    MAP_MOVE_LEFT,
    MOVE_GRASS_AND_CAR,
    LEVEL1,
    LEVEL2,
    LEVEL3,
    LEVEL4,
    LEVEL5
}