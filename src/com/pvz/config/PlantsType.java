/*
 *植物类型枚举
 */
package com.pvz.config;

public enum PlantsType {
    SUNFLOWER,
    PEASHOOTER,
    WALNUT,
    REPEATER,
    ICEPEA,
    CHERRYBOOM
}