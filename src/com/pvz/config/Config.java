/*
 *游戏初始设置
 */
package com.pvz.config;

public class Config {

    public static final int WINDOWS_WIDTH = 860;
    public static final int WINDOWS_HEIGHT = 645;
    public static final int PY_TOP = 30;
    public static final int PY_ILLUSTRATION = 30;
    public static int INIT_SUN = 3000;
    public static int INIT_HEAL = 5;
    public static int INIT_ADDATK = 5;
    public static int INIT_ADDSPEED = 5;
}