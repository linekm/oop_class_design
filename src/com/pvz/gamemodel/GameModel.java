/*
 *所有游戏物体的共同父类
 */

package com.pvz.gamemodel;

import java.awt.Image;

public abstract class GameModel {

    // Height Width speed x y

    public int Count() {
        int i = getT();
        i++;
        setT(i);
        return i;
    }

    private int t;

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    private int X;
    private int y;
    private Image GifImage;
    private Image image;
    private int Height;
    private int Width;
    private int speed;

    public Image getGifImage() {
        return GifImage;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setGifImage(Image gifImage) {
        GifImage = gifImage;
    }

    public int getHeight() {
        return Height;
    }

    public void setHeight(int height) {
        Height = height;
    }

    public int getWidth() {
        return Width;
    }

    public void setWidth(int width) {
        Width = width;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getX() {
        return X;
    }

    public void setX(int x) {
        X = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    private int HP;
    private int MaxHP;

    public int getMaxHP() {
        return MaxHP;
    }

    public void setMaxHP(int maxHP) {
        MaxHP = maxHP;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int hP) {
        HP = hP;
    }

    private boolean isDeathing;

    public boolean isDeathing() {
        return isDeathing;
    }

    public void setDeathing(boolean isDeathing) {
        this.isDeathing = isDeathing;
    }


}