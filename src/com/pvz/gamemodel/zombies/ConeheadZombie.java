package com.pvz.gamemodel.zombies;

import com.pvz.utils.ResourceUtils;

public class ConeheadZombie extends Zombie {
    public ConeheadZombie(int X) {
        setGifImage(ResourceUtils.GIFIMAGES.get("ConeheadZombie.gif").getImage());
        setHeight(getGifImage().getHeight(null));
        setWidth(getGifImage().getWidth(null));
        setX(X);
        setY(RanSetY() - getHeight() / 2);
        setHP(300);
        setMaxHP(300);
        setSpeed(100);
        setZombieAtk(10);
        setZbType(ZombieType.Conehead);
    }
}