package com.pvz.gamemodel.zombies;

import com.pvz.utils.ResourceUtils;

public class NormalZombie extends Zombie {

    public NormalZombie(int X) {
        setGifImage(ResourceUtils.GIFIMAGES.get("Zombie.gif").getImage());
        setHeight(getGifImage().getHeight(null));
        setWidth(getGifImage().getWidth(null));
        setX(X);
        setY(RanSetY() - getHeight() / 2);
        setHP(150);
        setMaxHP(150);
        setSpeed(80);
        setZombieAtk(8);
        setZbType(ZombieType.NORMAL);
    }
}