package com.pvz.gamemodel.plants;

import java.util.Random;

import com.pvz.config.*;
import com.pvz.utils.*;

public class Peashooter extends Plants {
    //	private ArrayList<Bullet> SingleBullet;
//	public ArrayList<Bullet> getSingleBullet() {
//		return SingleBullet;
//	}
//	public void setSingleBullet(ArrayList<Bullet> singleBullet) {
//		SingleBullet = singleBullet;
//	}
    Random r = new Random();

    public Peashooter() {
        setColdSpeed(600);
        setT(0);
        setHP(8000);
        setMaxHP(8000);
        //setAtk(25);
        setPlantstype(PlantsType.PEASHOOTER);
        setSun(100);
        setGifImage(ResourceUtils.GIFIMAGES.get("Peashooter.gif").getImage());
        setImage(ResourceUtils.IMAGES.get("Peashooter.png"));
        setChoicedImg(ResourceUtils.IMAGES.get("PeashooterChoiced2.png"));
        setWidth(getGifImage().getWidth(null));
        setHeight(getGifImage().getHeight(null));
    }


}