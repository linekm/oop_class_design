package com.pvz.gamemodel.plants;

import com.pvz.config.*;
import com.pvz.utils.*;

public class SnowPea extends Plants {
    public SnowPea() {
        setColdSpeed(600);
        setT(0);
        setHP(7000);
        setMaxHP(7000);
        //setAtk(25);
        setPlantstype(PlantsType.ICEPEA);
        setSun(175);
        setGifImage(ResourceUtils.GIFIMAGES.get("SnowPea.gif").getImage());
        setImage(ResourceUtils.IMAGES.get("SnowPea.png"));
        setChoicedImg(ResourceUtils.IMAGES.get("SnowPeaNo.png"));
        setWidth(getGifImage().getWidth(null));
        setHeight(getGifImage().getHeight(null));
    }
}