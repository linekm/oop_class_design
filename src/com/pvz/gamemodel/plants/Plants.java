/*
 *植物共同父类
 */
package com.pvz.gamemodel.plants;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.concurrent.ConcurrentHashMap;

import com.pvz.config.*;
import com.pvz.gamemodel.GameModel;
import com.pvz.gamemodel.bullet.Bullet;
import com.pvz.utils.*;

public abstract class Plants extends GameModel {
    private int ColdSpeed;
    private int adatk;
    private int adspeed;
    private PlantsType plantstype;
    private int Sun;
    private Image ChoicedImg;
    private boolean isChoiced;
    private int adindex;

    public int getColdSpeed() {
        return ColdSpeed;
    }

    public void setColdSpeed(int coldSpeed) {
        ColdSpeed = coldSpeed;
    }

    private boolean iscold;

    public boolean isIscold() {
        return iscold;
    }

    public void setIscold(boolean iscold) {
        this.iscold = iscold;
    }

    public static int GetColdTime(Plants p) {
        int n = 0;
        if (p.getPlantstype() == PlantsType.SUNFLOWER) {
            n = 100;
        } else if (p.getPlantstype() == PlantsType.REPEATER || p.getPlantstype() == PlantsType.PEASHOOTER) {
            n = 600;
        } else if (p.getPlantstype() == PlantsType.CHERRYBOOM) {
            n = 600;
        }
        return n;
    }

    public static int GetColdSpeed(Plants p) {
        int n = 0;
        if (p.getPlantstype() == PlantsType.SUNFLOWER) {
            n = 300;
        } else if (p.getPlantstype() == PlantsType.REPEATER || p.getPlantstype() == PlantsType.PEASHOOTER) {
            n = 400;
        } else if (p.getPlantstype() == PlantsType.ICEPEA) {
            n = 600;
        } else if (p.getPlantstype() == PlantsType.CHERRYBOOM) {
            n = 2500;
        } else if (p.getPlantstype() == PlantsType.WALNUT) {
            n = 2100;
        }
        return n;
    }

    public int getAdspeed() {
        return adspeed;
    }

    public void setAdspeed(int adspeed) {
        this.adspeed = adspeed;
    }

    public int getAdindex() {
        return adindex;
    }

    public void setAdindex(int adindex) {
        this.adindex = adindex;
    }

    public int getAdatk() {
        return adatk;
    }

    public void setAdatk(int adatk) {
        this.adatk = adatk;
    }

    public static ConcurrentHashMap<Bullet, MyPoint> BulletsList = new ConcurrentHashMap<Bullet, MyPoint>();

    public ConcurrentHashMap<Bullet, MyPoint> getBulletsList() {
        return BulletsList;
    }

    public void setBulletsList(ConcurrentHashMap<Bullet, MyPoint> bulletsList) {
        BulletsList = bulletsList;
    }


    public Image getChoicedImg() {
        return ChoicedImg;
    }

    public void setChoicedImg(Image choicedImg) {
        ChoicedImg = choicedImg;
    }


    public boolean isChoiced() {
        return isChoiced;
    }

    public void setChoiced(boolean isChoiced) {
        this.isChoiced = isChoiced;
    }

    public int getSun() {
        return Sun;
    }

    public void setSun(int sun) {
        Sun = sun;
    }

    public PlantsType getPlantstype() {
        return plantstype;
    }

    public void setPlantstype(PlantsType plantstype) {
        this.plantstype = plantstype;
    }

    public Rectangle GetRectangle() {
        Rectangle r = new Rectangle(getX(), getY(), getWidth(), getHeight());
        return r;
    }
}