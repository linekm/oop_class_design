package com.pvz.gamemodel.plants;

import java.awt.Rectangle;
import java.util.concurrent.ConcurrentHashMap;

import com.pvz.config.PlantsType;
import com.pvz.utils.MyPoint;
import com.pvz.utils.ResourceUtils;

public class SunFlower extends Plants {
    //public static ArrayList<MyPoint> FlowerSunList = new ArrayList<MyPoint>();
    public static ConcurrentHashMap<Sun, MyPoint> FlowerSunList = new ConcurrentHashMap<Sun, MyPoint>();

    public SunFlower() {
        //System.out.println(new Date().getSeconds()+"---------------------");
        setColdSpeed(200);
        setHP(4000);
        setT(0);
        setPlantstype(PlantsType.SUNFLOWER);
        setSun(50);
        setMaxHP(4000);
        setGifImage(ResourceUtils.GIFIMAGES.get("SunFlower.gif").getImage());
        setImage(ResourceUtils.IMAGES.get("sunflower.png"));
        setChoicedImg(ResourceUtils.IMAGES.get("choicedsunflower.png"));
        setWidth(getGifImage().getWidth(null));
        setHeight(getGifImage().getHeight(null));
    }

    public static boolean isEixt(MyPoint p) {
        boolean f = false;
        for (int i = 0; i < FlowerSunList.size(); i++) {
            if (FlowerSunList.get(i).getX() == p.getX() && FlowerSunList.get(i).getY() == p.getY()) {
                f = true;
            }
        }
        return f;
    }

    public static Rectangle GetRect(MyPoint p) {
        Rectangle rect = new Rectangle(p.getX(), p.getY(), 60, 60);

        return rect;
    }

    public static void drawSun(int x, int y) {
        MyPoint p = new MyPoint(x + 10, y - 20);
        if (!isEixt(p)) {
            //FlowerSunList.add(p);
        }
        System.out.println("FlowerSunList" + FlowerSunList.size());
    }
}
