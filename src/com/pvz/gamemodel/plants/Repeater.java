package com.pvz.gamemodel.plants;


import com.pvz.config.PlantsType;
import com.pvz.utils.ResourceUtils;

public class Repeater extends Plants {
    public Repeater() {
        setColdSpeed(600);
        setT(10);
        setHP(8000);
        setMaxHP(8000);
        setPlantstype(PlantsType.REPEATER);
        setSun(200);
        setGifImage(ResourceUtils.GIFIMAGES.get("Repeater.gif").getImage());
        setImage(ResourceUtils.IMAGES.get("Repeater.png"));
        setChoicedImg(ResourceUtils.IMAGES.get("RepeaterChoiced.png"));
        setWidth(getGifImage().getWidth(null));
        setHeight(getGifImage().getHeight(null));
    }
}