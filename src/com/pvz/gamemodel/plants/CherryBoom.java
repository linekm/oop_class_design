package com.pvz.gamemodel.plants;


import com.pvz.config.PlantsType;
import com.pvz.utils.ResourceUtils;

public class CherryBoom extends Plants {
    public CherryBoom() {
        setGifImage(ResourceUtils.GIFIMAGES.get("CherryBomb.gif").getImage());
        setImage(ResourceUtils.IMAGES.get("cherryboom.png"));
        setWidth(getImage().getWidth(null));
        setHeight(getImage().getHeight(null));
        setColdSpeed(600);
        setT(0);
        //setAtk(25);
        setPlantstype(PlantsType.CHERRYBOOM);
        setSun(150);
        setChoicedImg(ResourceUtils.IMAGES.get("choicedcherryboom.png"));
    }
}