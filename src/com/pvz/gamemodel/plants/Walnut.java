package com.pvz.gamemodel.plants;

import com.pvz.config.*;
import com.pvz.utils.*;

public class Walnut extends Plants {
    public Walnut() {
        setColdSpeed(600);
        setT(0);
        setHP(40000);
        setMaxHP(40000);
        //setAtk(25);
        setPlantstype(PlantsType.WALNUT);
        setSun(50);
        setGifImage(ResourceUtils.GIFIMAGES.get("WallNut.gif").getImage());
        setImage(ResourceUtils.IMAGES.get("Walnut.png"));
        setChoicedImg(ResourceUtils.IMAGES.get("ChoicedJG.png"));
        setWidth(getGifImage().getWidth(null));
        setHeight(getGifImage().getHeight(null));
    }
}