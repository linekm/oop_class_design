package com.pvz.gamemodel.plants;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import com.pvz.gamemodel.GameModel;
import com.pvz.utils.*;

public class Sun extends GameModel {
    Random r = new Random();

    public Sun() {
        setDate(new Date().getSeconds() + r.nextInt(10));
    }

    private int date;

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public static ArrayList<MyPoint> SunList = new ArrayList<MyPoint>();

    public ArrayList<MyPoint> getSunList() {
        return SunList;
    }

    public void setSunList(ArrayList<MyPoint> sunList) {
        SunList = sunList;
    }

    public Rectangle GetRectangle() {
        Rectangle r = new Rectangle(getX(), getY(), getWidth(), getHeight());
        return r;
    }
}