/*
 *攻击子弹实现类
 */

package com.pvz.gamemodel.bullet;

import java.awt.Rectangle;

import com.pvz.config.Config;
import com.pvz.gamemodel.GameModel;

public class Bullet extends GameModel {

    // GifImage  image ATK  bulletType
    private boolean playmusic;

    public boolean isPlaymusic() {
        return playmusic;
    }

    public void setPlaymusic(boolean playmusic) {
        this.playmusic = playmusic;
    }

    private int index;
    private int maxindex;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getMaxindex() {
        return maxindex;
    }

    public void setMaxindex(int maxindex) {
        this.maxindex = maxindex;
    }

    private int Atk;
    private BulletType bulletType;

    public int getAtk() {
        return Atk;
    }

    public void setAtk(int aTK) {
        Atk = aTK;
    }

    public BulletType getBulletType() {
        return bulletType;
    }

    public void setBulletType(BulletType bulletType) {
        this.bulletType = bulletType;
    }

    public boolean moveBullet() {
        boolean f = true;
        int x = getX() + getSpeed();
        //System.out.println(Config.WINDOWS_WIDTH-getWidth()+"============"+x);
        if (x > Config.WINDOWS_WIDTH - getWidth() - 5) {
            f = false;
        } else {
            setX(x);
        }
        return f;
    }

    public Rectangle GetRectangle() {
        Rectangle r = new Rectangle(getX(), getY(), getWidth(), getHeight());
        return r;
    }

}