package com.pvz.gamemodel.bullet;

public enum BulletType {
    PEA,
    DOUBLE_PEA,
    ICE_PEA
}