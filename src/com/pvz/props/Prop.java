package com.pvz.props;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.pvz.gamemodel.GameModel;

public class Prop extends GameModel {
    public Rectangle GetPropRectangle() {
        Rectangle r = new Rectangle(getX(), getY(), getWidth(), getHeight());
        return r;
    }

    public static ArrayList<Prop> propList = new ArrayList<>();
    public static ArrayList<Prop> addAtkPropList = new ArrayList<>();
    public static ArrayList<Prop> addSpeedPropList = new ArrayList<>();
    private PropType propType;

    public static ArrayList<Prop> getPropList() {
        return propList;
    }

    public static void setPropList(ArrayList<Prop> propList) {
        Prop.propList = propList;
    }

    public PropType getPropType() {
        return propType;
    }

    public void setPropType(PropType propType) {
        this.propType = propType;
    }
}