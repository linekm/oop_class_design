package com.pvz.props;

public enum PropType {
    HEAL, /* restore HP */
    ADDATK, /* add plant's attack */
    ADDSPEED /* decrease plant's fire interval */
}