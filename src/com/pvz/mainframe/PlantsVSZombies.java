/*
 *游戏主界面
 *游戏业务逻辑
 */
package com.pvz.mainframe;

import com.pvz.config.*;
import com.pvz.gamemodel.bullet.Bullet;
import com.pvz.gamemodel.bullet.BulletType;
import com.pvz.props.*;
import com.pvz.gamemodel.plants.*;
import com.pvz.utils.*;
import com.pvz.gamemodel.zombies.*;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;


public class PlantsVSZombies extends JFrame implements Runnable {
    public static GameState gameState = GameState.WELCOME;
    private static int HxdjIndex, LevelIndex, PropIndex, PointerUP, StartBtnIndex, SunIndex, MxmsStartIndex, IllustrationIndex, comeBackY, moveMapLeftIndex, TipsIndex, bgIndex, GrassWidth, CarWidth = 0;
    private static double imageWidth = 0;
    private int shovelIndex = 0;
    private static int SunXXX, SunYYY, RandomSunIndex = 0;
    Rectangle IllustrationRec = new Rectangle(Config.WINDOWS_WIDTH / 5 * 4, Config.WINDOWS_HEIGHT / 5 * 4, ResourceUtils.IMAGES.get("Illustration.png").getWidth(), ResourceUtils.IMAGES.get("Illustration.png").getHeight());
    private MyCanvas myCanvas;
    private int addAtkIndex, addSpeedIndex = 0;
    private int lastZombieX, lastZombieY = 0;
    private int addAtkPropIndex, addSpeedPropIndex = 0;
    private Prop prop;
    private static int SunY, SunX, RandomSunY = 0;
    private SunFlower sunflower = new SunFlower();
    private Peashooter peashooter = new Peashooter();
    private Repeater repeater = new Repeater();
    private SnowPea snowPea = new SnowPea();
    private CherryBoom cherryboom = new CherryBoom();
    private Walnut walnut = new Walnut();
    private Cursor handCursor;
    private Cursor arrowCursor;
    private int MouseX, MouseY = 0;
    private int x, y = 0;
    private int mouseIndex = 0;
    private int propX, propY;
    public static int boomX, prepare;
    public static int boomY;
    //File choiced = new File("./src/mp3/choiced.wav");
    File winer = new File("./src/mp3/winer.wav");
    File click = new File("./src/mp3/click.wav");
    File hit = new File("./src/mp3/hit.wav");
    File come = new File("./src/mp3/zombiecome.wav");
    File action = new File("./src/mp3/action.wav");
    File paint = new File("./src/mp3/paint.wav");
    File gaming = new File("./src/mp3/gaming.wav");
    File button = new File("./src/mp3/button.wav");
    private String image;
    private Rectangle PlayerChoicedPlantRect;
    static MyPoint p;
    int ZombieX = 0;
    MouseEvent e;
    int SunYY, SunXX = 0;
    private int atkindex = 0;
    ArrayList<MyPoint> point;
    private int buindex, bulletAtk, JianIndex;
    private int healPropIndex;
    private int bX, bY;

    public PlantsVSZombies() {
        setTitle("植物大战僵尸");
        setSize(Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        handCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
        arrowCursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
        myCanvas = new MyCanvas();
        Container c = getContentPane();
        c.add(myCanvas);
        MouseAda m = new MouseAda();
        c.addMouseMotionListener(m);
        c.addMouseListener(m);
        c.setFocusable(true);
        setVisible(true);
        setResizable(false);
    }

    /**
     * 该类负责画面渲染
     */
    class MyCanvas extends JPanel {

        int atkr, atkl = 0;

        /**
         * 渲染逻辑
         * @param g
         */
        @Override
        protected void paintComponent(Graphics g) {
            if (gameState == GameState.WELCOME) {
                drawWelcomeBG(g);
            } else if (gameState == GameState.MAIN_SCREEN) {
                drawMainScreenBG(g);
                drawcomback(g);
            } else if (gameState == GameState.SELECT_LEVEL) {
                drawSelectLevel(g);
            } else if (gameState == GameState.ILLUSTRATION) {
                //植物 僵尸图鉴
                // not implemented
            } else if (gameState == GameState.MAP_MOVE_RIGHT) {
                drawRightZombie(g);
                //地图背景移动
                drawMoveMapRight(g);
            } else if (gameState == GameState.MAP_MOVE_LEFT) {
                //背景地图左移
                drawMoveMapLeft(g);
            } else if (gameState == GameState.MOVE_GRASS_AND_CAR) {
                //移动草地和车
                drawMoveGrassAndCar(g);
                drawPlantsBoard(g);
            } else if (gameState == GameState.CHOOSE_PLANTS) {
                //选择植物
                drawPlantsBack(g);
                //选择植物
                drawPlantsBoard(g);

            } else if (gameState == GameState.LEVEL1 || gameState == GameState.LEVEL2 || gameState == GameState.LEVEL3
                    || gameState == GameState.LEVEL4 || gameState == GameState.LEVEL5) {
                //System.out.println("----------------------");
                //游戏开始
                drawGaming(g);
                //画plants集合中的 植物 由玩家选定
                drawPlantsBoard(g);
                //每隔15秒产生一个阳光
                drawSun(g);
                //玩家选择植物 植物跟随鼠标移动
                drawPlayerPlants(g);
                //将玩家选择的植物画入地图中
                drawPlantsInMap(g);
                //删除植物
                drawDeletePlants(g);
                //移动太阳
                drawSunMove(g);
                drawSunflowerSun(g);
                //子弹
                drawBullet(g);
                //僵尸
                drawZombie(g);
                //击杀zombie爆道具
                drawZombieProp(g);
                //收集阳光
                drawCollectSun(g);
                //
                drawGetPlants(g);
            } else if (gameState == GameState.WIN) {
                drawProp(g);
            } else if (gameState == GameState.GAMEOVER) {
                drawGameOver(g);
            }
        }

        /**
         * 僵尸死后生成道具
         * @param g
         */
        private void drawZombieProp(Graphics g) {
            // TODO Auto-generated method stub
            for (int i = 0; i < Prop.propList.size(); i++) {
                Prop prop = Prop.propList.get(i);
                g.drawImage(prop.getImage(), prop.getX(), prop.getY(), this);
            }
            for (int i = 0; i < Prop.addAtkPropList.size(); i++) {
                Prop prop = Prop.addAtkPropList.get(i);
                g.drawImage(prop.getImage(), prop.getX(), prop.getY(), this);
            }
            for (int i = 0; i < Prop.addSpeedPropList.size(); i++) {
                Prop prop = Prop.addSpeedPropList.get(i);
                g.drawImage(prop.getImage(), prop.getX(), prop.getY(), this);
            }
        }

        /**
         * 删除植物
         * @param g
         */
        private void drawDeletePlants(Graphics g) {
            // TODO Auto-generated method stub
            g.drawImage(ResourceUtils.IMAGES.get("ShovelBack.png"), 520, 12, this);
            g.drawImage(ResourceUtils.IMAGES.get("Shovel.png"), 520, 12, this);
        }

        /**
         * 选择关卡
         * @param g
         */
        private void drawSelectLevel(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("ChooseLevel.png"), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT, this);
            g.setColor(Color.black);
            for (int i = 0; i < 5; i++) {
                g.drawRect(258 + i * 99, 141, 77, 30);
            }
        }

        /**
         * 渲染道具
         * @param g
         */
        private void drawProp(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("AwardScreen_Back.jpg"), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT, this);
            g.drawImage(ResourceUtils.IMAGES.get(image), 380, 150, this);

        }

        /**
         * 渲染右侧僵尸
         * @param g
         */
        private void drawRightZombie(Graphics g) {
            for (int i = 0; i < LoadingPlants.LoadingZombieLists.size(); i++) {
                Zombie zb = LoadingPlants.LoadingZombieLists.get(i);
                g.drawImage(zb.getGifImage(), 0, 0, zb.getWidth(), zb.getHeight(), this);
            }
        }

        /**
         * 渲染关卡末获取植物
         * @param g
         */
        private void drawGetPlants(Graphics g) {
            if (LoadingPlants.LoadingZombieLists.size() == 1) {
                Zombie zb = LoadingPlants.LoadingZombieLists.get(0);
                lastZombieX = zb.getX();
                lastZombieY = zb.getY();
            }
            if (LoadingPlants.LoadingZombieLists.size() == 0) {
                if (LevelIndex == 1) {
                    g.drawImage(ResourceUtils.IMAGES.get("Repeater2.png"), lastZombieX, lastZombieY, this);
                    image = "Repeater2.png";
                }
            }
        }

        /**
         * 游戏失败时界面
         * @param g
         */
        private void drawGameOver(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("ZombiesWon.png"), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT, this);
        }

        /**
         * 收集太阳时行为
         * @param g
         */
        private void drawCollectSun(Graphics g) {
            g.setFont(new Font(Font.DIALOG, Font.BOLD, 20));
            g.drawString("" + Config.INIT_SUN, 50, 85);
        }

        /**
         * 渲染子弹行为
         * @param g
         */
        private void drawBullet(Graphics g) {
            super.repaint();
            if (Plants.BulletsList.size() > 0) {
                Set<Entry<Bullet, MyPoint>> entrySet = Plants.BulletsList.entrySet();
                for (Entry<Bullet, MyPoint> entry : entrySet) {
                    g.drawImage(entry.getKey().getGifImage(), entry.getKey().getX(), entry.getValue().getY(), this);

                }
            }

            if (JianIndex == 1) {
                if (bulletAtk >= 10) {
                    atkr = bulletAtk % 10;
                    atkl = bulletAtk / 10 % 10;
                    g.drawImage(ResourceUtils.IMAGES.get("RedLine.png"), bX - 5, bY - 31, this);
                    g.drawImage(ResourceUtils.IMAGES.get(atkl + ".png"), bX + 20, bY - 45, this);
                    g.drawImage(ResourceUtils.IMAGES.get(atkr + ".png"), bX + 38, bY - 45, this);
                } else {
                    g.drawImage(ResourceUtils.IMAGES.get("RedLine.png"), bX - 5, bY - 31, this);
                    g.drawImage(ResourceUtils.IMAGES.get(bulletAtk + ".png"), bX + 20, bY - 45, this);
                }
                Timer t = new Timer();
                t.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        JianIndex = 0;
                    }
                }, 200);
            }

        }

        /**
         * 渲染僵尸行为
         * @param g
         */
        private void drawZombie(Graphics g) {
            if (LoadingPlants.LoadingZombieLists.size() > 0) {
                for (int i = 0; i < LoadingPlants.LoadingZombieLists.size(); i++) {
                    Zombie zb = LoadingPlants.LoadingZombieLists.get(i);
                    if (!zb.isDeathing() && zb.getDie() != 1) {
                        g.drawImage(zb.getGifImage(), zb.getX(), zb.getY(), zb.getWidth(), zb.getHeight(), this);
                        int w = (int) (zb.getHP() * 1.0 / zb.getMaxHP() * zb.getGifImage().getWidth(null));
                        g.setColor(Color.red);
                        g.drawRect(zb.getX() - 2, zb.getY() - 8, zb.getGifImage().getWidth(null), 6);
                        g.fillRect(zb.getX() - 2, zb.getY() - 8, w, 6);
                    }
                    if (zb.isDeathing() && zb.getDie() != 1) {
                        g.drawImage(zb.getZombieDie(), zb.getX(), zb.getY(), zb.getZombieDie().getWidth(null), zb.getZombieDie().getHeight(null), this);
                    }

                    if (zb.getDie() == 1 && zb.isDeathing()) {
                        g.drawImage(ResourceUtils.GIFIMAGES.get("BoomDie2.gif").getImage(), zb.getX() - 80, zb.getY(), ResourceUtils.GIFIMAGES.get("BoomDie2.gif").getImage().getWidth(null), ResourceUtils.GIFIMAGES.get("BoomDie2.gif").getImage().getHeight(null), this);
                    }

                    if (buindex == 1) {
                        g.drawImage(ResourceUtils.GIFIMAGES.get("PeaBulletHit.gif").getImage(), bX - 10, bY + 60, this);
                        Timer t = new Timer();
                        t.schedule(new TimerTask() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                buindex = 0;
                            }
                        }, 30);
                    }
                }
            }
        }

        /**
         * 渲染向日葵行为
         * @param g
         */
        private void drawSunflowerSun(Graphics g) {
            if (SunFlower.FlowerSunList.size() > 0) {
                Iterator<Entry<Sun, MyPoint>> it = SunFlower.FlowerSunList.entrySet().iterator();
                while (it.hasNext()) {
                    Entry<Sun, MyPoint> b = it.next();
                    g.drawImage(b.getKey().getGifImage(), b.getValue().getX(), b.getValue().getY(), this);
                }
            }

        }

        /**
         * 游戏开始，初始化背景
         * @param g
         */
        private void drawWelcomeBG(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("menu.png"), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT - Config.PY_TOP, null);
            if (StartBtnIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("gamestart.png"), Config.WINDOWS_WIDTH / 2 - ResourceUtils.IMAGES.get("gamestart.png").getWidth() / 2 - 12, 560, 322, ResourceUtils.IMAGES.get("gamestart.png").getHeight(), this);
            } else {
                g.drawImage(ResourceUtils.IMAGES.get("gamestart1.png"), Config.WINDOWS_WIDTH / 2 - ResourceUtils.IMAGES.get("gamestart1.png").getWidth() / 2 - 12, 560, 322, ResourceUtils.IMAGES.get("gamestart.png").getHeight(), this);
            }
        }

        /**
         * 游戏主菜单渲染
         * @param g
         */
        private void drawMainScreenBG(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("ZombieStart.png"), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT, this);
            if (MxmsStartIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("mxms2.png"), Config.WINDOWS_WIDTH / 2 + ResourceUtils.IMAGES.get("mxms2.png").getWidth() / 2, 100, this);
            } else {
                g.drawImage(ResourceUtils.IMAGES.get("mxms.png"), Config.WINDOWS_WIDTH / 2 + ResourceUtils.IMAGES.get("mxms.png").getWidth() / 2, 100, this);
            }
            if (IllustrationIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("Illustration.png"), Config.WINDOWS_WIDTH / 5 * 4, Config.WINDOWS_HEIGHT / 5 * 4 - Config.PY_ILLUSTRATION, this);
            } else {
                g.drawImage(ResourceUtils.IMAGES.get("Illustration2.png"), Config.WINDOWS_WIDTH / 5 * 4, Config.WINDOWS_HEIGHT / 5 * 4 - Config.PY_ILLUSTRATION, this);
            }
        }

        /**
         * "欢迎回来"栏渲染
         * @param g
         */
        private void drawcomback(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("comeback.png"), Config.WINDOWS_WIDTH / 6, -ResourceUtils.IMAGES.get("comeback.png").getHeight() + comeBackY, this);
        }

        /**
         * 画图鉴
         * @param g
         */
        private void drawIllustration(Graphics g) {

        }

        /**
         * 植物栏
         * @param g
         */
        private void drawPlantsBoard(Graphics g) {
            Factory f = new Factory();
            g.drawImage(ResourceUtils.IMAGES.get("SeedBank.png"), 30, 5, this);
            for (int i = 0; i < PlantsBar.PlantsBarList.size(); i++) {
                if (f.ColdTime(PlantsBar.PlantsBarList.get(i), gameState) > 0) {
                    g.drawImage(PlantsBar.PlantsBarList.get(i).getChoicedImg(), 103 + i * 52, 12, 50, 72, this);
                    PlantsBar.PlantsBarList.get(i).setIscold(true);
                } else {
                    g.drawImage(PlantsBar.PlantsBarList.get(i).getImage(), 103 + i * 52, 12, 50, 72, this);
                    PlantsBar.PlantsBarList.get(i).setIscold(false);
                }

                if (gameState == GameState.GAMING || gameState == GameState.LEVEL1 && f.ColdTime(PlantsBar.PlantsBarList.get(i), gameState) != 0) {
                    setFont(new Font("微软雅黑", Font.BOLD, 20));
                    g.setColor(Color.WHITE);
                    g.drawString("" + f.ColdTime(PlantsBar.PlantsBarList.get(i), gameState), 103 + i * 52 + 23, 50);
                }
            }
        }

        /**
         * 渲染植物花园
         * @param g
         */
        private void drawPlantsBack(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("background1unsodded.jpg").getSubimage(200, 0, Config.WINDOWS_WIDTH, ResourceUtils.IMAGES.get("background1unsodded_1.jpg").getHeight()), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT - Config.PY_TOP, this);
            g.drawImage(ResourceUtils.GIFIMAGES.get("Dave.gif").getImage(), 0, 85, this);
            if (TipsIndex == 0) {
                g.drawImage(ResourceUtils.IMAGES.get("SeedBank.png"), 30, 5, this);
                g.drawImage(ResourceUtils.IMAGES.get("SeedChooser_Background.png"), ResourceUtils.GIFIMAGES.get("Dave.gif").getIconWidth(), ResourceUtils.IMAGES.get("SeedBank.png").getHeight() + 50, 470, 400, this);
                g.drawImage(ResourceUtils.IMAGES.get("go.png"), 550, 480, this);
                g.drawImage(ResourceUtils.IMAGES.get("reset.png"), 480, 480, this);

                for (int j = 0; j < LoadingPlants.PlantsList.size(); j++) {
                    if (j <= 6) {

                        if (LoadingPlants.PlantsList.get(j).isChoiced()) {
                            g.drawImage(LoadingPlants.PlantsList.get(j).getChoicedImg(), 310 + j * 65, 163, 55, 80, this);
                        } else {
                            g.drawImage(LoadingPlants.PlantsList.get(j).getImage(), 310 + j * 65, 163, 55, 80, this);
                        }
                    } else {
                        g.drawImage(LoadingPlants.PlantsList.get(j).getImage(), 310 + (j - 7) * 65, 163 + 85, 55, 80, this);
                    }
                }
            }
        }

        /**
         * 背景地图右移
         * @param g
         */
        private void drawMoveMapRight(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("background1unsodded.jpg").getSubimage(180 + bgIndex, 0, Config.WINDOWS_WIDTH, ResourceUtils.IMAGES.get("background1unsodded.jpg").getHeight()), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT - Config.PY_TOP, this);
            g.drawImage(ResourceUtils.GIFIMAGES.get("Dave.gif").getImage(), 0, 85, this);
            g.drawImage(ResourceUtils.IMAGES.get("prompt.png"), Config.WINDOWS_WIDTH / 2 - ResourceUtils.IMAGES.get("prompt.png").getWidth() / 4, 85, ResourceUtils.IMAGES.get("prompt.png").getWidth() / 3 * 2, ResourceUtils.IMAGES.get("prompt.png").getHeight() / 2, this);
        }

        /**
         * 背景地图左移
         * @param g
         */
        private void drawMoveMapLeft(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("background1unsodded.jpg").getSubimage(540 - moveMapLeftIndex, 0, Config.WINDOWS_WIDTH, ResourceUtils.IMAGES.get("background1unsodded.jpg").getHeight()), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT - Config.PY_TOP, this);
            g.drawImage(ResourceUtils.GIFIMAGES.get("Dave.gif").getImage(), 0, 85, this);
        }

        private void drawMoveGrassAndCar(Graphics g) {
            //画背景
            g.drawImage(ResourceUtils.IMAGES.get("background1unsodded.jpg").getSubimage(200, 0, Config.WINDOWS_WIDTH, ResourceUtils.IMAGES.get("background1unsodded_1.jpg").getHeight()), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT - Config.PY_TOP, this);
            //画  人物 退出
            g.drawImage(ResourceUtils.GIFIMAGES.get("Dave2.gif").getImage(), 0, 85, this);
            //画  草 逐渐向右展开
            g.drawImage(ResourceUtils.IMAGES.get("sod1row.png"), 48, Config.WINDOWS_HEIGHT / 2 - 47, GrassWidth, ResourceUtils.IMAGES.get("sod1row.png").getHeight(), this);
            //画 土 逐渐向右展开
            g.drawImage(ResourceUtils.IMAGES.get("SodRoll1.png"), 25 + GrassWidth, Config.WINDOWS_HEIGHT / 2 - 40, (int) (80 - imageWidth), 90, this);
            //画  草 逐渐向右展开
            g.drawImage(ResourceUtils.IMAGES.get("SodRollCap.png"), 25 + GrassWidth, Config.WINDOWS_HEIGHT / 2, (int) (80 - imageWidth), 75, this);
            //画  车 逐渐向右展开
            g.drawImage(ResourceUtils.GIFIMAGES.get("LawnMower.gif").getImage(), -ResourceUtils.GIFIMAGES.get("LawnMower.gif").getIconWidth() + CarWidth, Config.WINDOWS_HEIGHT / 2 - 40, this);

        }

        /**
         * 游戏开始场景绘制
         * @param g
         */
        private void drawGaming(Graphics g) {
            g.drawImage(ResourceUtils.IMAGES.get("background1.jpg").getSubimage(200, 0, Config.WINDOWS_WIDTH, ResourceUtils.IMAGES.get("background1.jpg").getHeight()), 0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT - Config.PY_TOP, this);

            g.drawImage(ResourceUtils.GIFIMAGES.get("LawnMower.gif").getImage(), 0, Config.WINDOWS_HEIGHT / 2 - 40, this);

            if (prepare == 0) {
                g.drawImage(ResourceUtils.GIFIMAGES.get("PrepareGrowPlants.gif").getImage(), 300, 300, ResourceUtils.GIFIMAGES.get("PrepareGrowPlants.gif").getIconWidth(), ResourceUtils.GIFIMAGES.get("PrepareGrowPlants.gif").getIconHeight(), this);
                Timer t = new Timer();
                t.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        prepare = 1;
                    }
                }, 3000);
            }

            if (PointerUP == 0) {
                g.drawImage(ResourceUtils.GIFIMAGES.get("PointerUP.gif").getImage(), 110, 90, this);
            } else if (PointerUP == 1 && DrawPlantsInMap.PlantsMap.size() <= 0) {
                g.drawImage(ResourceUtils.GIFIMAGES.get("PointerDown.gif").getImage(), 80, 110, this);

            }

            g.drawImage(ResourceUtils.IMAGES.get("FlagMeterLevelProgress.png"), 680, 580, 157, 20, this);

            int w = (int) (LoadingPlants.LoadingZombieLists.size() * 1.0 / LoadingPlants.zombiesize * ResourceUtils.IMAGES.get("FlagMeterFull.png").getWidth());
            g.drawImage(ResourceUtils.IMAGES.get("FlagMeterEmpty.png"), 680, 560, this);

            if (w <= 0) {
                g.drawImage(ResourceUtils.IMAGES.get("FlagMeterFull.png").getSubimage(0, 0, 157, 21), 676 + w - 1, 560, this);

            } else {
                g.drawImage(ResourceUtils.IMAGES.get("FlagMeterFull.png").getSubimage(5, 0, 158 - w, 21), 676 + w - 1, 560, this);
            }
            g.drawImage(ResourceUtils.IMAGES.get("FlagMeterParts2.png"), 676 + w, 545, this);

            g.drawImage(ResourceUtils.IMAGES.get("PropBark.png"), 650, 12, this);
            g.drawImage(ResourceUtils.IMAGES.get("Heal.png"), 652, 12, this);
            g.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
            g.drawString("剩余治疗道具" + Config.INIT_HEAL + "个", 685, 31);

            g.drawImage(ResourceUtils.IMAGES.get("PropBark.png"), 650, 46, this);
            g.drawImage(ResourceUtils.IMAGES.get("addatk.png"), 652, 46, this);
            g.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
            g.drawString("剩余加攻道具" + Config.INIT_ADDATK + "个", 685, 68);

            g.drawImage(ResourceUtils.IMAGES.get("PropBark.png"), 650, 80, this);
            g.drawImage(ResourceUtils.IMAGES.get("addspeed.png"), 652, 80, this);
            g.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
            g.drawString("剩余攻速道具" + Config.INIT_ADDSPEED + "个", 685, 100);

        }

        /**
         * 令植物跟随鼠标移动
         * @param g
         */
        private void drawPlayerPlants(Graphics g) {
            if (PlayerChoicedPlantRect != null) {
                g.drawImage(PlantsBar.GetPlayerPlants(PlayerChoicedPlantRect).getGifImage(), MouseX - 30, MouseY - 35, this);
            }
            if (shovelIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("Shovel.png"), MouseX - 30, MouseY - 20, this);
            }
            if (HxdjIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("Heal.png"), MouseX - 20, MouseY - 20, this);
            }
            if (addAtkIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("addatk.png"), MouseX - 20, MouseY - 20, this);
            }
            if (addSpeedIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("addspeed.png"), MouseX - 20, MouseY - 20, this);
            }
        }

        /**
         * 将玩家选择的植物画入地图中
         * @param g
         */
        private void drawPlantsInMap(Graphics g) {
            Set<Entry<Plants, MyPoint>> entrySet = DrawPlantsInMap.PlantsMap.entrySet();
            for (Entry<Plants, MyPoint> entry : entrySet) {
                Plants p = entry.getKey();
                if (entry.getKey().getPlantstype() != PlantsType.CHERRYBOOM) {
                    g.drawImage(p.getGifImage(), p.getX(), entry.getValue().getY(), this);
                    int w = (int) (p.getHP() * 1.0 / p.getMaxHP() * p.getGifImage().getWidth(null));
                    g.setColor(Color.red);
                    g.drawRect(entry.getValue().getX() - 2, entry.getValue().getY() - 8, p.getGifImage().getWidth(null),
                            6);
                    g.fillRect(entry.getValue().getX() - 2, entry.getValue().getY() - 8, w, 6);
                } else {
                    g.drawImage(p.getGifImage(), p.getX(), entry.getValue().getY(), this);
                    Timer t = new Timer();
                    t.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            DrawPlantsInMap.PlantsMap.remove(p);

                        }
                    }, 500);
                    g.drawImage(ResourceUtils.GIFIMAGES.get("Boom.gif").getImage(), p.getX() - 50, entry.getValue().getY(), this);

                    g.drawRect(boomX - 100, boomY - 100, 200, 200);
                }
            }
        }

        /**
         * 随机产生的太阳
         * @param g
         */
        private void drawSun(Graphics g) {
            if (Sun.SunList.size() > 0 && RandomSunIndex != 1) {
                g.drawImage(ResourceUtils.GIFIMAGES.get("Sun.gif").getImage(), Sun.SunList.get(0).getX(), -ResourceUtils.GIFIMAGES.get("Sun.gif").getIconHeight() + SunY, 60, 60, this);
            }
        }

        /**
         * 拾取太阳
         */
        private void drawSunMove(Graphics g) {
            if (SunIndex == 1) {
                g.drawImage(ResourceUtils.GIFIMAGES.get("Sun.gif").getImage(), SunXX, SunYY, 60, 60, this);
            }
            if (RandomSunIndex == 1) {
                g.drawImage(ResourceUtils.GIFIMAGES.get("Sun.gif").getImage(), SunXXX, SunYYY, 60, 60, this);
            }
            if (healPropIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("Heal.png"), propX, propY, this);
            }
            if (addAtkPropIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("addatk.png"), propX, propY, this);
            }
            if (addSpeedPropIndex == 1) {
                g.drawImage(ResourceUtils.IMAGES.get("addspeed.png"), propX, propY, this);
            }
        }
    }

    /**
     * 该类负责鼠标事件监听
     */
    class MouseAda extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            //System.out.println(Thread.currentThread().getName());
            if (gameState == GameState.WELCOME) {
                //获取开始按钮 ，当点击时 更改游戏状态为WELECOMETWO
                Rectangle ActionRect = new Rectangle(Config.WINDOWS_WIDTH / 2 - ResourceUtils.IMAGES.get("gamestart.png").getWidth() / 2, 545, ResourceUtils.IMAGES.get("gamestart.png").getWidth(), ResourceUtils.IMAGES.get("gamestart.png").getHeight());
                if (ActionRect.contains(e.getPoint())) {
                    gameState = GameState.MAIN_SCREEN;
                }
            } else if (gameState == GameState.MAIN_SCREEN) {
                //获取  冒险模式按钮 位置，当点击时 更改游戏状态为GAMING
                Rectangle MxmsRect = new Rectangle(Config.WINDOWS_WIDTH / 2 + ResourceUtils.IMAGES.get("mxms.png").getWidth() / 2, 100, ResourceUtils.IMAGES.get("mxms.png").getWidth(), ResourceUtils.IMAGES.get("mxms.png").getHeight());
                if (MxmsRect.contains(e.getPoint())) {
                    //更该游戏状态  为MAP_MOVE_RIGHT  背景地图向右移动
                    SoundUtils.Play(button, false, GameState.MAIN_SCREEN);
                    SoundUtils.Play(action, false, GameState.MAIN_SCREEN);
                    Timer t = new Timer();
                    t.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            gameState = GameState.SELECT_LEVEL;
                        }
                    }, 2000);
                }
                if (IllustrationRec.contains(e.getPoint())) {
                    //玩家点击 图鉴按钮  更改游戏状态  ILLUSTRATION
                    gameState = GameState.ILLUSTRATION;
                    //System.out.println(gameState);
                }
            } else if (gameState == GameState.SELECT_LEVEL) {
                LoadingPlants l = new LoadingPlants();
                if (l.ChocedGuanQia(e) == 1) {
                    LevelIndex = 1;
                    //SoundUtils.Play(choiced,true,GameState.MAP_MOVE_RIGHT);
                    SoundUtils.Play(ResourceUtils.AUDIO.get("choiced.wav"), true, GameState.MAP_MOVE_RIGHT);
                    //SoundUtils.Play(choiced,true,GameState.MAP_MOVE_LEFT);
                    gameState = GameState.MAP_MOVE_RIGHT;
                } else if (l.ChocedGuanQia(e) == 2) {
                    LevelIndex = 2;
                    SoundUtils.Play(ResourceUtils.AUDIO.get("choiced.wav"), true, GameState.MAP_MOVE_RIGHT);
                    gameState = GameState.MAP_MOVE_RIGHT;
                } else if (l.ChocedGuanQia(e) == 3) {
                    LevelIndex = 3;
                    SoundUtils.Play(ResourceUtils.AUDIO.get("choiced.wav"), true, GameState.MAP_MOVE_RIGHT);
                    gameState = GameState.MAP_MOVE_RIGHT;
                } else if (l.ChocedGuanQia(e) == 4) {
                    LevelIndex = 4;
                    SoundUtils.Play(ResourceUtils.AUDIO.get("choiced.wav"), true, GameState.MAP_MOVE_RIGHT);
                    gameState = GameState.MAP_MOVE_RIGHT;
                } else if (l.ChocedGuanQia(e) == 5) {
                    LevelIndex = 5;
                    SoundUtils.Play(ResourceUtils.AUDIO.get("choiced.wav"), true, GameState.MAP_MOVE_RIGHT);
                    gameState = GameState.MAP_MOVE_RIGHT;
                }
            } else if (gameState == GameState.MAP_MOVE_RIGHT) {
                Rectangle AllRec = new Rectangle(0, 0, Config.WINDOWS_WIDTH, Config.WINDOWS_HEIGHT);
                if (AllRec.contains(e.getPoint())) {

                    gameState = GameState.MAP_MOVE_LEFT;

                }
            } else if (gameState == GameState.CHOOSE_PLANTS) {

                //玩家选择植物
                Rectangle goRec = new Rectangle(550, 480, 65, 37);
                LoadingPlants.ChoicedPlants(mouseIndex, LoadingPlants.sunflowerRect, sunflower, e);
                LoadingPlants.ChoicedPlants(mouseIndex, LoadingPlants.wandouRect, peashooter, e);
                LoadingPlants.ChoicedPlants(mouseIndex, LoadingPlants.DoubleWaRect, repeater, e);
                LoadingPlants.ChoicedPlants(mouseIndex, LoadingPlants.snowPea, snowPea, e);
                LoadingPlants.ChoicedPlants(mouseIndex, LoadingPlants.cherryboom, cherryboom, e);
                LoadingPlants.ChoicedPlants(mouseIndex, LoadingPlants.walnut, walnut, e);
                if (goRec.contains(e.getPoint())) {
                    //点击 GO图标  更改游戏状态为MOVE_GRASS_AND_CAR   移动车和草地
                    gameState = GameState.MOVE_GRASS_AND_CAR;
                }
            } else if (gameState == GameState.GAMING || gameState == GameState.LEVEL1 || gameState == GameState.LEVEL2 || gameState == GameState.LEVEL3 || gameState == GameState.LEVEL4 || gameState == GameState.LEVEL5) {

                for (int i = 0; i < Prop.propList.size(); i++) {
                    prop = Prop.propList.get(i);
                    if (prop.GetPropRectangle().contains(e.getPoint())) {
                        propX = prop.getX();
                        propY = prop.getY();
                        healPropIndex = 1;
                        Config.INIT_HEAL = Config.INIT_HEAL + 1;
                        Prop.propList.remove(prop);
                    }
                }
                for (int i = 0; i < Prop.addAtkPropList.size(); i++) {
                    prop = Prop.addAtkPropList.get(i);
                    if (prop.GetPropRectangle().contains(e.getPoint())) {
                        propX = prop.getX();
                        propY = prop.getY();
                        addAtkPropIndex = 1;
                        Config.INIT_HEAL = Config.INIT_ADDATK + 1;
                        Prop.addAtkPropList.remove(prop);
                    }
                }
                for (int i = 0; i < Prop.addSpeedPropList.size(); i++) {
                    prop = Prop.addSpeedPropList.get(i);
                    if (prop.GetPropRectangle().contains(e.getPoint())) {
                        propX = prop.getX();
                        propY = prop.getY();
                        addSpeedPropIndex = 1;
                        Config.INIT_HEAL = Config.INIT_ADDSPEED + 1;
                        Prop.addSpeedPropList.remove(prop);
                    }
                }

                Rectangle hxdjRect = new Rectangle(652, 12, 24, 24);
                Rectangle addatkjRect = new Rectangle(652, 46, 24, 24);
                Rectangle addspeedjRect = new Rectangle(652, 80, 24, 24);
                if (hxdjRect.contains(e.getPoint()) && Config.INIT_HEAL > 0) {
                    HxdjIndex = 1;
                }
                if (addatkjRect.contains(e.getPoint()) && Config.INIT_ADDATK > 0) {
                    addAtkIndex = 1;
                }
                if (addspeedjRect.contains(e.getPoint()) && Config.INIT_ADDSPEED > 0) {
                    addSpeedIndex = 1;
                }
                Rectangle rr = new Rectangle(SunX, SunY - 60, 60, 60);
                if (e.getButton() == 3) {
                    HxdjIndex = 0;
                    addAtkIndex = 0;
                    addSpeedIndex = 0;
                    shovelIndex = 0;
                }
                if (rr.contains(e.getPoint())) {
                    SunXXX = e.getX();
                    SunYYY = e.getY();
                    RandomSunIndex = 1;
                    Config.INIT_SUN = Config.INIT_SUN + 25;
                    //System.out.println("太阳被点击");
                    SoundUtils.Play(click, false, gameState);
                    //SunY = 0;
                }
                Rectangle djrect = new Rectangle(lastZombieX, lastZombieY, 100, 60);
                if (djrect.contains(e.getPoint()) && LoadingPlants.LoadingZombieLists.size() <= 0) {
                    //SoundUtils.Play(ResourceUtils.AUDIO.get("winner.wav"), false, GameState.LEVEL1);
                    PropIndex = 1;
                }
                Rectangle CzRect = new Rectangle(520, 12, 76, 34);
                if (CzRect.contains(e.getPoint())) {
                    shovelIndex = 1;
                }
                Rectangle CRect = new Rectangle(MouseX, MouseY, 76, 34);
                Rectangle HXect = new Rectangle(MouseX, MouseY, 24, 24);
                Set<Entry<Plants, MyPoint>> entrySet = DrawPlantsInMap.PlantsMap.entrySet();
                for (Entry<Plants, MyPoint> entry : entrySet) {
                    Plants p = entry.getKey();
                    if (p.GetRectangle().intersects(CRect) && shovelIndex == 1) {
                        DrawPlantsInMap.PlantsMap.remove(p);
                        shovelIndex = 0;
                    }
                    if (p.GetRectangle().intersects(HXect) && HxdjIndex == 1) {
                        if (p.getMaxHP() < p.getHP() + 4000) {
                            p.setHP(p.getMaxHP());
                        } else {
                            p.setHP(p.getHP() + 4000);
                        }
                        Config.INIT_HEAL--;
                        HxdjIndex = 0;
                    }
                    if (p.GetRectangle().intersects(HXect) && addAtkIndex == 1) {
                        p.setAdatk(1);
                        Config.INIT_ADDATK--;
                        addAtkIndex = 0;
                    }
                    if (p.GetRectangle().intersects(HXect) && addSpeedIndex == 1) {
                        p.setAdspeed(1);
                        Config.INIT_ADDSPEED--;
                        addSpeedIndex = 0;
                    }

                }
                for (int i = 0; i < LoadingPlants.LoadingZombieLists.size(); i++) {
                    Zombie zb = LoadingPlants.LoadingZombieLists.get(i);
                    if (zb.GetRectangle().intersects(CRect) && shovelIndex == 1) {
                        LoadingPlants.LoadingZombieLists.remove(zb);
                        shovelIndex = 0;
                    }
                }

                Iterator<Entry<Sun, MyPoint>> it = SunFlower.FlowerSunList.entrySet().iterator();
                while (it.hasNext()) {
                    Entry<Sun, MyPoint> b = it.next();
                    if (b.getKey().GetRectangle().contains(e.getPoint())) {
                        SunXX = b.getValue().getX();
                        SunYY = b.getValue().getY();
                        SunIndex = 1;
                        //System.out.println("太阳------被点");
                        SoundUtils.Play(click, false, gameState);
                        Config.INIT_SUN = Config.INIT_SUN + 25;
                        it.remove();
                    }
                }

                Rectangle rec = new Rectangle(50, 80, 730, 520);
                Rectangle PlantsBarRec = new Rectangle(103, 12, 350, 72);
                for (int i = 0; i < PlantsBar.PlantsBarList.size(); i++) {
                    //System.out.println(PlantsBar.PlantsBarList.get(i).isIscold()+"===================");
                    if (PlantsBarRec.contains(e.getPoint()) && !PlantsBar.PlantsBarList.get(i).isIscold() && Config.INIT_SUN >= PlantsBar.PlantsBarList.get(i).getSun()) {
                        PlayerChoicedPlantRect = PlantsBar.GetRect(e);
                        PlantsBar.GetPlayerPlants(PlayerChoicedPlantRect).setColdSpeed(0);
                        PlantsBar.PlantsBarList.get(i).setIscold(true);
                        setCursor(handCursor);
                        PointerUP = 1;
                    }
                }

                if (rec.contains(e.getPoint()) && PlayerChoicedPlantRect != null) {
                    boomX = e.getX();
                    boomY = e.getY();
                    x = (e.getX() - 52) / 81 * 81 + 60;
                    y = (e.getY() - 100) / 100 * 100 + 100;
                    MyPoint getpoint = new MyPoint(x, y);
                    Plants player = PlantsBar.GetPlayerPlants(PlayerChoicedPlantRect);
                    Plants MapPlants = null;
                    switch (player.getPlantstype()) {
                    case SUNFLOWER:
                        MapPlants = new SunFlower();
                        MapPlants.setX(x);
                        MapPlants.setY(y);

                        break;
                    case PEASHOOTER:
                        MapPlants = new Peashooter();
                        MapPlants.setX(x);
                        MapPlants.setY(y);

                        break;
                    case REPEATER:
                        MapPlants = new Repeater();
                        MapPlants.setX(x);
                        MapPlants.setY(y);

                        break;
                    case ICEPEA:
                        MapPlants = new SnowPea();
                        MapPlants.setX(x);
                        MapPlants.setY(y);
                        break;
                    case CHERRYBOOM:
                        MapPlants = new CherryBoom();
                        MapPlants.setX(x);
                        MapPlants.setY(y);
                        break;
                    case WALNUT:
                        MapPlants = new Walnut();
                        MapPlants.setX(x);
                        MapPlants.setY(y);
                        break;
                    default:
                        break;
                    }
                    if (DrawPlantsInMap.IsExitPlant(getpoint)) {

                        Config.INIT_SUN = Config.INIT_SUN - MapPlants.getSun();
                        if (Config.INIT_SUN < 0) {
                            //JOptionPane.showMessageDialog(null, "钱不够","警告",JOptionPane.ERROR_MESSAGE);
                            Config.INIT_SUN = Config.INIT_SUN + MapPlants.getSun();
                        } else {
                            SoundUtils.Play(paint, false, gameState);
                            DrawPlantsInMap.PlantsMap.put(MapPlants, getpoint);
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "这块地种过植物了", "警告", JOptionPane.ERROR_MESSAGE);
                    }
                    PlayerChoicedPlantRect = null;
                    setCursor(arrowCursor);
                    //System.out.println(DrawPlantsInMap.PlantsMap.size());
                }
            } else if (gameState == GameState.GAMEOVER) {
                //获取开始按钮 ，当点击时 更改游戏状态为WELECOMETWO
                LoadingPlants.LoadingZombieLists.clear();
                gameState = GameState.MAIN_SCREEN;
            } else if (gameState == GameState.WIN) {
                LoadingPlants.LoadingZombieLists.clear();
                gameState = GameState.SELECT_LEVEL;
            }
            //System.out.println(e.getX()+"===="+e.getY());
            //myCanvas.repaint();
            myCanvas.updateUI();
        }

        @Override
        public void mouseMoved(MouseEvent e) {

            if (gameState == GameState.WELCOME) {
                Rectangle StartRect = new Rectangle(Config.WINDOWS_WIDTH / 2 - ResourceUtils.IMAGES.get("gamestart.png").getWidth() / 2, 545, ResourceUtils.IMAGES.get("gamestart.png").getWidth(), ResourceUtils.IMAGES.get("gamestart.png").getHeight());
                if (StartRect.contains(e.getPoint())) {
                    setCursor(handCursor);
                    StartBtnIndex = 1;
                } else {
                    setCursor(arrowCursor);
                    StartBtnIndex = 0;
                }
            }
            if (gameState == GameState.MAIN_SCREEN) {
                Rectangle WelcomeRect = new Rectangle(Config.WINDOWS_WIDTH / 2 + ResourceUtils.IMAGES.get("mxms.png").getWidth() / 2, 100, ResourceUtils.IMAGES.get("mxms.png").getWidth(), ResourceUtils.IMAGES.get("mxms.png").getHeight());
                if (WelcomeRect.contains(e.getPoint())) {
                    setCursor(handCursor);
                    MxmsStartIndex = 1;
                } else {
                    setCursor(arrowCursor);
                    MxmsStartIndex = 0;
                }
                if (IllustrationRec.contains(e.getPoint())) {
                    setCursor(handCursor);
                    IllustrationIndex = 1;
                } else {
                    setCursor(arrowCursor);
                    IllustrationIndex = 0;
                }
            }
            if (gameState == GameState.CHOOSE_PLANTS) {
                Rectangle goRec = new Rectangle(550, 480, 65, 37);
                Rectangle sunflowerRect = new Rectangle(310, 163, 55, 80);
                Rectangle wandouRect = new Rectangle(375, 163, 55, 80);
                if (goRec.contains(e.getPoint()) || wandouRect.contains(e.getPoint()) || sunflowerRect.contains(e.getPoint())) {
                    setCursor(handCursor);
                } else {
                    setCursor(arrowCursor);
                }
            }
            if (gameState == GameState.MOVE_GRASS_AND_CAR) {
                setCursor(arrowCursor);
            }

            if (gameState == GameState.GAMING || gameState == GameState.LEVEL1 || gameState == GameState.LEVEL2 || gameState == GameState.LEVEL3 || gameState == GameState.LEVEL4 || gameState == GameState.LEVEL5) {
                MouseX = e.getX();
                MouseY = e.getY();
            }
            //myCanvas.repaint();
            myCanvas.updateUI();
        }
    }

    @Override
    public void run() {
        while (true) {
            if (gameState == GameState.MAIN_SCREEN) {
                comback();
            }
            if (gameState == GameState.MAP_MOVE_RIGHT) {
                //System.out.println(bgIndex);
                mapMoveRight();
            }
            if (gameState == GameState.MAP_MOVE_LEFT) {
                mapMoveLeft();
            }
            if (gameState == GameState.MOVE_GRASS_AND_CAR) {
                moveGrass();
            }
            if (SunIndex == 1) {
                moveSun();
            }
            if (RandomSunIndex == 1) {
                RandomSunMove();
            }
            if (PropIndex == 1) {
                moveProp();
            }
            if (healPropIndex == 1) {
                moveHealProp();
            }
            if (addAtkPropIndex == 1) {
                moveAddAtk();
            }
            if (addSpeedPropIndex == 1) {
                moveAddSpeed();
            }

            if (gameState == GameState.GAMING || gameState == GameState.LEVEL1 || gameState == GameState.LEVEL2 || gameState == GameState.LEVEL3 || gameState == GameState.LEVEL4 || gameState == GameState.LEVEL5) {
                //Count();
                DrawPlantsInMap.AcdMapCreatBullet();

                //createSun();
                if (Plants.BulletsList.size() > 0) {
                    moveBullet();
                }
//				SoundUtils.Play(f, false, gameState);
                moveZombie();
            }
            if (gameState == GameState.GAMING || gameState == GameState.LEVEL1 || gameState == GameState.LEVEL2 || gameState == GameState.LEVEL3 || gameState == GameState.LEVEL4 || gameState == GameState.LEVEL5 && Sun.SunList.size() <= 0) {
                SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        Thread.sleep(12000);
                        createSun();
                        movesun();
                        return null;
                    }
                };
                worker.execute();
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }

    }

    private void moveAddAtk() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {
                    double jd = 90 - Math.atan2(46 - propY, 652 - propX) * 180 / Math.PI;
                    propX = (int) (propX + Math.sin(jd * Math.PI / 180) * 8);
                    propY = (int) (propY + Math.cos(jd * Math.PI / 180) * 8);
                    if (propY <= 46) {
                        addAtkPropIndex = 0;
                        break;
                    }
                    break;
                }
            }
        }).start();
    }

    private void moveAddSpeed() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {
                    double jd = 90 - Math.atan2(80 - propY, 652 - propX) * 180 / Math.PI;
                    propX = (int) (propX + Math.sin(jd * Math.PI / 180) * 8);
                    propY = (int) (propY + Math.cos(jd * Math.PI / 180) * 8);
                    if (propY <= 80) {
                        addSpeedPropIndex = 0;
                        break;
                    }
                    break;
                }
            }
        }).start();
    }

    private void moveHealProp() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    double jd = 90 - Math.atan2(12 - propY, 652 - propX) * 180 / Math.PI;
                    propX = (int) (propX + Math.sin(jd * Math.PI / 180) * 8);
                    propY = (int) (propY + Math.cos(jd * Math.PI / 180) * 8);
                    if (propY <= 12) {
                        healPropIndex = 0;
                        break;
                    }
                    break;
                }
            }
        }).start();
    }

    private void moveProp() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {
                    double jd = 90 - Math.atan2(152 - lastZombieY, 385 - lastZombieX) * 180 / Math.PI;
                    lastZombieX = (int) (lastZombieX + Math.sin(jd * Math.PI / 180) * 1.2);
                    lastZombieY = (int) (lastZombieY + Math.cos(jd * Math.PI / 180) * 1.2);
                    try {
                        Thread.sleep(2000);
                        gameState = GameState.WIN;
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    break;
                }
            }
        }).start();
    }

    private void moveZombie() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Factory.ZbMove();
            }
        }).start();
    }

    /**
     * 子弹移动
     */
    private void moveBullet() {
        Iterator<Map.Entry<Bullet, MyPoint>> it = Plants.BulletsList.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Bullet, MyPoint> b = it.next();
            if (!b.getKey().moveBullet()) {
                it.remove();
            } else {
                for (int i = 0; i < LoadingPlants.LoadingZombieLists.size(); i++) {
                    Zombie zb = LoadingPlants.LoadingZombieLists.get(i);
                    if (zb.isDeathing() == false && b.getKey().GetRectangle().intersects(zb.GetRectangle())) {
                        Factory.bulletAtkZombie(zb, b.getKey());
                        SoundUtils.Play(hit, false, GameState.LEVEL1);
                        bX = zb.getX();
                        bY = zb.getY();
                        bulletAtk = b.getKey().getAtk();
                        buindex = 1;
                        JianIndex = 1;
                        try {
                            it.remove();
                        } catch (Exception e) {
                        }

                    }
                    if (zb.isDeathing() == false && b.getKey().getBulletType() == BulletType.ICE_PEA && b.getKey().GetRectangle().intersects(zb.GetRectangle())) {
                        Factory.bulletAtkZombie(zb, b.getKey());
                        //System.out.println(zb.getSpeed());
                        zb.setSpeed(zb.getSpeed() + 100);
                        //System.out.println(zb.getSpeed());
                        new Thread(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(1000);
                                    zb.setSpeed(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                }
            }
        }
    }

    /**
     * 地图右移
     */
    private void mapMoveRight() {
        bgIndex = bgIndex + 4;
        if (bgIndex >= 360) {
            bgIndex = 360;
        }
    }

    /**
     * 地图左移
     */
    private void mapMoveLeft() {
        moveMapLeftIndex = moveMapLeftIndex + 4;
        if (moveMapLeftIndex >= 355) {
            moveMapLeftIndex = 355;
            gameState = GameState.CHOOSE_PLANTS;
            SoundUtils.Play(ResourceUtils.AUDIO.get("choiced.wav"), true, GameState.CHOOSE_PLANTS);
            //SoundUtils.Play(choiced, false, GameState.CHOOSE_PLANTS);
        }
    }

    /**
     * 点击太阳  太阳移动
     */
    private void moveSun() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    double jd = 90 - Math.atan2(20 - SunYY, 25 - SunXX) * 180 / Math.PI;
                    SunXX = (int) (SunXX + Math.sin(jd * Math.PI / 180) * 8);
                    SunYY = (int) (SunYY + Math.cos(jd * Math.PI / 180) * 8);
                    if (SunXX <= 20 || SunYY <= 25) {
                        SunIndex = 0;
                        break;
                    }
                    break;
                }
            }
        }).start();
    }

    /**
     * 随机的太阳移动
     */
    private void RandomSunMove() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //System.out.println(RandomSunIndex);
                while (true) {
                    double jd = 90 - Math.atan2(20 - SunYYY, 25 - SunXXX) * 180 / Math.PI;
                    SunXXX = (int) (SunXXX + Math.sin(jd * Math.PI / 180) * 8);
                    SunYYY = (int) (SunYYY + Math.cos(jd * Math.PI / 180) * 8);
                    if (SunXXX <= 20 || SunYYY <= 25) {
                        RandomSunIndex = 0;
                        break;
                    }
                    break;
                }
            }
        }).start();
    }

    /**
     * 每隔十秒产生一个阳光
     */
    public void createSun() {
        if (Sun.SunList.size() <= 0 && RandomSunIndex != 1 && SunIndex != 1) {
            Random r = new Random();
            SunX = r.nextInt(500) + 100;
            RandomSunY = r.nextInt(300) + 200;
            p = new MyPoint(SunX, RandomSunY);
            Sun.SunList.add(p);
        }
    }

    public void movesun() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(50);
                        SunY = SunY + 1;
                        //break;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (RandomSunIndex == 1) {
                        SunY = 0;
                        break;
                    }
                    if (SunY >= RandomSunY) {
                        SunY = RandomSunY;
                    }
                }
            }
        }).start();
    }

    /**
     * 游戏开始 草地移动
     */
    private void moveGrass() {
        GrassWidth = GrassWidth + 5;
        CarWidth++;
        imageWidth = imageWidth + 0.5;
        if (GrassWidth >= ResourceUtils.IMAGES.get("sod1row.png").getWidth()) {
            //break;
            GrassWidth = ResourceUtils.IMAGES.get("sod1row.png").getWidth();
            if (LevelIndex == 1) {
                gameState = GameState.LEVEL1;
                //
                //SoundUtils.Play(ResourceUtils.AUDIO.get("1.wav"), false, GameState.LEVEL1);
                SoundUtils.Play(gaming, false, GameState.LEVEL1);
            } else if (LevelIndex == 2) {
                gameState = GameState.LEVEL2;
            }
            if (LevelIndex == 3) {
                gameState = GameState.LEVEL3;
            }
            if (LevelIndex == 4) {
                gameState = GameState.LEVEL4;
            }
            if (LevelIndex == 5) {
                gameState = GameState.LEVEL5;
            }
            LoadingPlants.CreatZombie();
        }
        if (CarWidth >= ResourceUtils.GIFIMAGES.get("LawnMower.gif").getIconWidth()) {
            CarWidth = ResourceUtils.GIFIMAGES.get("LawnMower.gif").getIconWidth();
        }
        if (imageWidth >= 60) {
            imageWidth = 60;
        }
    }

    /**
     * "欢迎回来"栏
     */
    private void comback() {
        comeBackY++;
        if (comeBackY >= ResourceUtils.IMAGES.get("comeback.png").getHeight()) {
            comeBackY = ResourceUtils.IMAGES.get("comeback.png").getHeight();
        }
    }

    //Main
    public static void main(String[] args) {
        PlantsVSZombies UI = new PlantsVSZombies();
        Thread t = new Thread(UI);
        t.start();
        File f = new File("./src/mp3/welcome.wav");
        SoundUtils.playMusic(true, f, GameState.WELCOME);
    }
}